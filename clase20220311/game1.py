from json.tool import main
import pygame

pygame.init()

# FPS: Frame per seconds
# Surface: Es donde dibujamos

# RGB
WHITE = (255, 255, 255)
BLUE = (0, 0, 255)
YELLOW = (255, 255, 0)
GREEN = (0,200, 0)

FPS = 60
clock = pygame.time.Clock()

main_surface = pygame.display.set_mode((300, 450))
main_surface.fill(GREEN)

pygame.draw.line(main_surface, WHITE, (100, 400), (200, 400), 7)

in_game = True
while in_game:

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            in_game = False

    pygame.display.update()
    
    clock.tick(FPS)

